> usage 

Cette petite application s'inscrit dans le cadre de BDAA en Master I2L.
Elle a pour but de mettre en place un ORM. 

J'ai créé une classe abstraire qui gère toute la partie commune liée à la base de données (AbstractRepository), cette classe abstraite inclus toutes les fonctionnalités de bases d'une entité (save, get, delete, getList), ainsi tous les repository héritant de `AbstractRepository` possède ces méthodes.

une classe abstraite `AbstractEntity` permet de faire le lien entre une entité et sert à définir des Abstract entity.

L'idée est que toutes nouvelles entités, ou tous nouveaux repository héritent de ces deux classes afin de simplifier la gestion des contenus. 

Les constantes PK et Table contenues dans les classes Entity que l'on créée permettent de définir dans quelle table et quelle est la primary key de l'entité.