<?php

namespace Core\Repository;

use Core\Entity\AbstractEntity;
use Core\Connection;
use ReflectionProperty;

abstract class AbstractRepository{
    private $reflectionClass;
    private $properties;
    private $ENTITY_CLASS;
    private $name;

    public function __construct($entity)
    {
        $this->ENTITY_CLASS = $entity; 
        $this->reflectionClass = new \ReflectionClass($entity);
        $this->properties = $this->reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PRIVATE);
        $this->name = $this->reflectionClass->getConstant('TABLE_NAME');
    }

    public function save(AbstractEntity $AbstractEntity)
    {
        $this->getPrimaryKeyValue($AbstractEntity) ? $this->update($AbstractEntity) : $this->insert($AbstractEntity);
    }

    private function insert (AbstractEntity $entity) {
        $request = 'INSERT INTO ' . $this->name . ' ';
        $this->prepareRequest($entity, $request, true);
    }

    private function update (AbstractEntity $entity) {
        $request = 'UPDATE ' . $this->name . ' SET ';
        $this->prepareRequest($entity, $request, false);
    }

    private function prepareRequest(AbstractEntity $entity,string $request, $insert = true) {
        $array = [];
        $toBind = [];
        foreach ($this->properties as $propertie) {
            if ($propertie->getName() !== $this->reflectionClass->getConstant('PK')) {
                $propertie->setAccessible(true);
                $array[$propertie->getName()] = '?';
                $toBind[] = $propertie->getValue($entity);
            } else {
                $pkValue = $propertie->getValue($entity);
            }
        }

        $fields = implode(', ', array_keys($array));
        $values = implode(', ', array_values($array));

        if ($insert) {
            $request .= '(' . $fields . ') VALUES (' . $values . ')';
        } else {
            $request .= '(' . $fields . ') = (' . $values . ') WHERE ' . $this->reflectionClass->getConstant('PK') . ' = ' . $pkValue;
        }

        $this->executePreparedRequest($request, $toBind);
    }

    private function getPrimaryKeyValue(AbstractEntity $entity) {
        $fieldName = $this->reflectionClass->getConstant('PK');
        return (new ReflectionProperty($this->ENTITY_CLASS, $fieldName))->getValue($entity);
    }

    private function executePreparedRequest(string $request, Array $toBind) {
        $pdo = Connection::getConnection();
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $request = $pdo->prepare($request);
        $request->execute($toBind);
    }

    public function get(int $id) {
        $request = 'SELECT * FROM ' . $this->name . ' WHERE id = ' . $id;
        $pdo = Connection::getConnection();
        $request = $pdo->query($request);
        $request->setFetchMode(\PDO::FETCH_CLASS, $this->reflectionClass->getName());
        $res = $request->fetch();
        return $res;
    }

    public function getList()
    {
        $request = 'SELECT * FROM ' . $this->name;
        $pdo = Connection::getConnection();
        $request = $pdo->query($request);
        $request->setFetchMode(\PDO::FETCH_CLASS, $this->reflectionClass->getName());
        $res = $request->fetchAll();
        return $res;
    }

    public function delete(AbstractEntity $entity) {
        delete($entity->id);
    }

    public function deleteById(int $id) {
        $request = 'DELETE FROM ' . $this->name . ' WHERE id = ' . $id;
        $pdo = Connection::getConnection();
        $request = $pdo->query($request);
        $request->setFetchMode(\PDO::FETCH_CLASS, $this->reflectionClass->getName());
        $res = $request->fetch();
        return $res;
    }
}