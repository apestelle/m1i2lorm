<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once('Autoload.php');
Autoload::register();

use App\Entity\ProductEntity;
use App\Repository\ProductRepository;

use App\Entity\CustomerEntity;
use App\Repository\CustomerRepository;


$productRepository = new ProductRepository();
$customerRepository = new CustomerRepository();
// $product = new ProductEntity();
// $product->setName("bottes");
// $product->setPrice(10);

// $productRepository->save($product);

// $product = $productRepository->get(1);

echo '<h3> Liste produits : </h3>';
foreach($productRepository->getList() as $data)
{
    echo ('<p> Name : <b>' . $data->getName() . '</b> Price : <b>' . $data->getPrice() . '</b> € </p>');
}


$customer = new CustomerEntity();

// $customer->setFirstName('Alexis');
// $customer->setLastName('Pestelle');

// $customerRepository->save($customer);


echo '<h3> Liste Customer : </h3>';
foreach($customerRepository->getList() as $data)
{
    echo ('<p> FirstName : <b>' . $data->getFirstName() . '</b> LastName : <b>' . $data->getLastName() . '</b></p>');
}
