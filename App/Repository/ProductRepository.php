<?php

namespace App\Repository;

use App\Entity\ProductEntity;
use Core\Connection;
Use Core\Repository\AbstractRepository;

class ProductRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct(ProductEntity::class);
    }
}