<?php

namespace App\Repository;

use App\Entity\CustomerEntity;
use Core\Connection;
Use Core\Repository\AbstractRepository;

class CustomerRepository extends AbstractRepository
{
    public function __construct()
    {
        parent::__construct(CustomerEntity::class);
    }
}