<?php

namespace App\Entity;
use Core\Entity\AbstractEntity;

class ProductEntity extends AbstractEntity
{
    const TABLE_NAME = 'product';
    const PK = 'id';
    public $id;
    public $name;
    public $price;

    public function getId(): ?int{
        return $this->id;
    }

    public function setId(int $id): void{
        $this->id = $id;
    }

    public function getName(): string{
        return $this->name;
    }

    public function setName(string $name):void{
        $this->name = $name;
    }

    public function getPrice(): int{
        return $this->price;
    }

    public function setPrice(int $price){
        $this->price = $price;
    }
}